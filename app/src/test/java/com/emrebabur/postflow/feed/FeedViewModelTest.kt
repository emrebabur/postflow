package com.emrebabur.postflow.feed

import com.emrebabur.postflow.repo.post.PostRepo
import com.emrebabur.postflow.wrap
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class FeedViewModelTest {

    private val repoPosts: List<com.emrebabur.postflow.repo.post.Post> = List(10) {
        com.emrebabur.postflow.repo.post.Post("$it", "user $it", "title $it", "body $it")
    }

    private val feedPosts: List<Post> = repoPosts.map { Post(it.id, it.title, it.body) }

    @Test
    fun `repo has posts - states should be loading then displayed`() {
        val repo: PostRepo = mock()
        whenever(repo.get()).thenReturn(Single.just(repoPosts))
        val scheduler = TestScheduler()
        val viewModel = FeedViewModel(repo, scheduler, scheduler)

        val observer = viewModel.wrap().test()
        scheduler.triggerActions()

        assertThat(observer.values())
            .hasSize(2)
            .containsExactly(
                FeedViewModel.State.Loading,
                FeedViewModel.State.Displaying(feedPosts)
            )
    }
}
