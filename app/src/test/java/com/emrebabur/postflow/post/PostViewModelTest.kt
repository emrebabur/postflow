package com.emrebabur.postflow.post

import com.emrebabur.postflow.repo.post.PostRepo
import com.emrebabur.postflow.repo.user.User
import com.emrebabur.postflow.repo.user.UserRepo
import com.emrebabur.postflow.wrap
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import io.reactivex.schedulers.TestScheduler
import org.assertj.core.api.Assertions
import org.junit.Test

class PostViewModelTest {

    private val repoPost = com.emrebabur.postflow.repo.post.Post("1", "1", "title", "body")
    private val repoUser = User("1", "user")
    private val postDetail = Post("title", "user", "body")

    @Test
    fun `repo has post and user - state should be loading and displaying`() {
        val postRepo: PostRepo = mock()
        val userRepo: UserRepo = mock()
        whenever(postRepo.get(any())).thenReturn(Single.just(repoPost))
        whenever(userRepo.get(any())).thenReturn(Single.just(repoUser))
        val scheduler = TestScheduler()
        val viewModel = PostViewModel("1", postRepo, userRepo, scheduler, scheduler)

        val observer = viewModel.wrap().test()
        scheduler.triggerActions()

        Assertions.assertThat(observer.values())
            .hasSize(2)
            .containsExactly(
                PostViewModel.State.Loading,
                PostViewModel.State.Displaying(postDetail)
            )
    }
}
