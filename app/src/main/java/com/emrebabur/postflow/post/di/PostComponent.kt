package com.emrebabur.postflow.post.di

import com.emrebabur.postflow.ActivityScope
import com.emrebabur.postflow.application.AppComponent
import com.emrebabur.postflow.post.PostViewModelFactory
import com.emrebabur.postflow.post.comment.CommentViewModelFactory
import dagger.Component

@ActivityScope
@Component(
    dependencies = [AppComponent::class],
    modules = [PostModule::class]
)
interface PostComponent {

    fun postViewModelFactory(): PostViewModelFactory

    fun commentViewModelFactory(): CommentViewModelFactory
}