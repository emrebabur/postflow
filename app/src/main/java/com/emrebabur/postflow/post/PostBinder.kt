package com.emrebabur.postflow.post

import android.content.Context
import com.emrebabur.postflow.R
import com.emrebabur.postflow.setVisibility
import io.reactivex.functions.Consumer

class PostBinder(private val holder: PostViewHolder) : Consumer<PostViewModel.State> {

    override fun accept(model: PostViewModel.State) {
        with(holder) {
            loadingSpinner.setVisibility(model is PostViewModel.State.Loading)
            if(model is PostViewModel.State.Displaying) {
                with(model) {
                    title.text = post.title
                    author.text = getAuthorText(author.context, post.author)
                    body.text = post.body
                }
            }
        }
    }

    private fun getAuthorText(context: Context, name: String) =
        context.getString(R.string.author_label, name)
}
