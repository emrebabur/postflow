package com.emrebabur.postflow.post.comment

data class Comment(
    val id: String,
    val name: String,
    val email: String,
    val body: String
)
