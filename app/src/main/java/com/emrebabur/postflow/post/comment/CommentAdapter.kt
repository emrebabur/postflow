package com.emrebabur.postflow.post.comment

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.emrebabur.postflow.R
import com.emrebabur.postflow.inflate

class CommentAdapter : RecyclerView.Adapter<CommentAdapter.CommentViewModelViewHolder>() {

    private var items: MutableList<Comment> = mutableListOf()

    fun setItems(newItems: List<Comment>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): CommentViewModelViewHolder =
        CommentViewModelViewHolder(parent.inflate(R.layout.post_comment))

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: CommentViewModelViewHolder, position: Int) {
        holder.model = items[position]
    }

    inner class CommentViewModelViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val name = view.findViewById<TextView>(R.id.post_detail_commentAuthorName)
        private val email = view.findViewById<TextView>(R.id.post_detail_commentAuthorEmail)
        private val body = view.findViewById<TextView>(R.id.post_detail_commentBody)


        var model: Comment? = null
            set(value) {
                field = value?.also {
                    name.text = it.name
                    email.text = it.email
                    body.text = it.body
                }
            }
    }
}
