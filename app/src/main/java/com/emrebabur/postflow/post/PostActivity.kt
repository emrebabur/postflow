package com.emrebabur.postflow.post

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.emrebabur.postflow.R
import com.emrebabur.postflow.application.PostFlowApp
import com.emrebabur.postflow.plusAssign
import com.emrebabur.postflow.post.comment.CommentFeedBinder
import com.emrebabur.postflow.post.comment.CommentFeedViewHolder
import com.emrebabur.postflow.post.comment.CommentFeedViewModel
import com.emrebabur.postflow.post.di.DaggerPostComponent
import com.emrebabur.postflow.post.di.PostComponent
import com.emrebabur.postflow.post.di.PostModule
import com.emrebabur.postflow.wrap
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import me.eugeniomarletti.extras.intent.IntentExtra
import me.eugeniomarletti.extras.intent.base.String

class PostActivity : AppCompatActivity() {

    private lateinit var component: PostComponent
    private lateinit var postViewModel: PostViewModel
    private lateinit var commentFeedViewModel: CommentFeedViewModel

    private lateinit var postBinder: PostBinder
    private lateinit var commentFeedBinder: CommentFeedBinder

    private val disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component = createComponent(intent.postId)
        val view = layoutInflater.inflate(R.layout.activity_post_detail, null)
        setContentView(view)
        postViewModel = ViewModelProviders.of(this, component.postViewModelFactory()).get(PostViewModel::class.java)
        postBinder = PostBinder(PostViewHolder(view))
        commentFeedViewModel = ViewModelProviders.of(this, component.commentViewModelFactory()).get(CommentFeedViewModel::class.java)
        commentFeedBinder = CommentFeedBinder(CommentFeedViewHolder(view))
    }

    override fun onStart() {
        super.onStart()
        disposable += postViewModel.wrap()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(postBinder)

        disposable += commentFeedViewModel.wrap()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(commentFeedBinder)
    }

    override fun onStop() {
        disposable.clear()
        super.onStop()
    }

    private fun createComponent(postId: String): PostComponent =
        DaggerPostComponent.builder()
            .appComponent(PostFlowApp.appComponent)
            .postModule(PostModule(postId))
            .build()

    companion object {

        internal var Intent.postId by IntentExtra.String(
            reader = { it ?: error("post id missing in Intent") },
            writer = { it }
        )

        fun start(context: AppCompatActivity, postId: String) {
            val intent = Intent(context, PostActivity::class.java).apply {
                this.postId = postId
            }
            context.startActivity(intent)
        }
    }
}
