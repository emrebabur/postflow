package com.emrebabur.postflow.post.comment

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.emrebabur.postflow.R

class CommentFeedViewHolder(view: View) {
    val commentCount: TextView = view.findViewById(R.id.post_detail_commentCount)
    val recyclerView: RecyclerView = view.findViewById(R.id.post_detail_commentRecyclerView)
}
