package com.emrebabur.postflow.post

data class Post(
    val title: String,
    val author: String,
    val body: String
)
