package com.emrebabur.postflow.post.comment

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.emrebabur.postflow.plusAssign
import com.emrebabur.postflow.repo.comment.CommentRepo
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.ObservableSource
import io.reactivex.Observer
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable

class CommentViewModelFactory(
    private val postId: String,
    private val commentRepo: CommentRepo,
    private val ioScheduler: Scheduler,
    private val mainScheduler: Scheduler
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        CommentFeedViewModel(postId, commentRepo, ioScheduler, mainScheduler) as T
}

class CommentFeedViewModel(
    postId: String,
    commentRepo: CommentRepo,
    ioScheduler: Scheduler,
    mainScheduler: Scheduler
) : ViewModel(), ObservableSource<CommentFeedViewModel.State> {

    private val disposable = CompositeDisposable()
    private val stateRelay: Relay<State> = BehaviorRelay.create()

    init {
        disposable += commentRepo.get(postId)
            .map {
                it.map {
                    Comment(it.id, it.name, it.email, it.body)
                }
            }
            .map { State.Displaying(it) as State }
            .subscribeOn(ioScheduler)
            .observeOn(mainScheduler)
            .toObservable()
            .startWith(State.Loading)
            .subscribe(stateRelay)
    }

    override fun onCleared() {
        disposable.clear()
        super.onCleared()
    }

    override fun subscribe(observer: Observer<in CommentFeedViewModel.State>) {
        stateRelay.subscribe(observer)
    }

    sealed class State {
        object Loading : State()
        data class Displaying(val comments: List<Comment>) : State()
    }
}
