package com.emrebabur.postflow.post

import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import com.emrebabur.postflow.R

class PostViewHolder(view: View) {
    val title: TextView = view.findViewById(R.id.post_detail_title)
    val author: TextView = view.findViewById(R.id.post_detail_author)
    val body: TextView = view.findViewById(R.id.post_detail_body)
    val loadingSpinner: ProgressBar = view.findViewById(R.id.post_detail_postLoadingSpinner)
}
