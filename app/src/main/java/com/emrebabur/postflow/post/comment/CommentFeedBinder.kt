package com.emrebabur.postflow.post.comment

import android.support.v7.widget.LinearLayoutManager
import io.reactivex.functions.Consumer

class CommentFeedBinder(private val holder: CommentFeedViewHolder) :
    Consumer<CommentFeedViewModel.State> {

    private val layoutManager = LinearLayoutManager(holder.recyclerView.context)
    private val adapter = CommentAdapter()

    init {
        holder.recyclerView.layoutManager = layoutManager
        holder.recyclerView.adapter = adapter
    }

    override fun accept(state: CommentFeedViewModel.State) {
        if (state is CommentFeedViewModel.State.Displaying) {
            holder.commentCount.text = state.comments.size.toString()
            adapter.setItems(state.comments)
        }
    }
}
