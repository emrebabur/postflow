package com.emrebabur.postflow.post.di

import com.emrebabur.postflow.ActivityScope
import com.emrebabur.postflow.application.SchedulerModule
import com.emrebabur.postflow.post.PostViewModelFactory
import com.emrebabur.postflow.post.comment.CommentViewModelFactory
import com.emrebabur.postflow.repo.comment.CommentRepo
import com.emrebabur.postflow.repo.post.PostRepo
import com.emrebabur.postflow.repo.user.UserRepo
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Named

@Module
class PostModule(private val postId: String) {

    @Provides
    @ActivityScope
    fun postViewModelFactory(
        postRepo: PostRepo,
        userRepo: UserRepo,
        @Named(SchedulerModule.KEY_SCHEDULER_IO) ioScheduler: Scheduler,
        @Named(SchedulerModule.KEY_SCHEDULER_MAIN) mainScheduler: Scheduler
    ): PostViewModelFactory =
        PostViewModelFactory(postId, postRepo, userRepo, ioScheduler, mainScheduler)

    @Provides
    @ActivityScope
    fun commentViewModelFactory(
        commentRepo: CommentRepo,
        @Named(SchedulerModule.KEY_SCHEDULER_IO) ioScheduler: Scheduler,
        @Named(SchedulerModule.KEY_SCHEDULER_MAIN) mainScheduler: Scheduler
    ): CommentViewModelFactory =
        CommentViewModelFactory(postId, commentRepo, ioScheduler, mainScheduler)
}
