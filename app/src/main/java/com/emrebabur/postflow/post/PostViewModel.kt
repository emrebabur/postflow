package com.emrebabur.postflow.post

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.emrebabur.postflow.plusAssign
import com.emrebabur.postflow.repo.post.PostRepo
import com.emrebabur.postflow.repo.user.UserRepo
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.ObservableSource
import io.reactivex.Observer
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable

class PostViewModelFactory(
    private val postId: String,
    private val postRepo: PostRepo,
    private val userRepo: UserRepo,
    private val ioScheduler: Scheduler,
    private val mainScheduler: Scheduler
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        PostViewModel(postId, postRepo, userRepo, ioScheduler, mainScheduler) as T
}


class PostViewModel(
    postId: String,
    private val postRepo: PostRepo,
    private val userRepo: UserRepo,
    ioScheduler: Scheduler,
    mainScheduler: Scheduler
) : ViewModel(), ObservableSource<PostViewModel.State> {

    private val disposable = CompositeDisposable()
    private val stateRelay: Relay<State> = BehaviorRelay.create()

    init {
        disposable += getPost(postId)
            .map { State.Displaying(it) as State }
            .subscribeOn(ioScheduler)
            .observeOn(mainScheduler)
            .toObservable()
            .startWith(State.Loading)
            .subscribe(stateRelay)
    }

    override fun onCleared() {
        disposable.clear()
        super.onCleared()
    }

    override fun subscribe(observer: Observer<in PostViewModel.State>) {
        stateRelay.subscribe(observer)
    }

    private fun getPost(postId: String) =
        postRepo.get(postId)
            .flatMap { post ->
                userRepo.get(post.userId)
                    .map { user ->
                        Post(post.title, user.name, post.body)
                    }
            }

    sealed class State {
        object Loading : State()
        data class Displaying(val post: Post) : State()
    }
}
