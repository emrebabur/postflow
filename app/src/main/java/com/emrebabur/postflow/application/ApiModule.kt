package com.emrebabur.postflow.application

import com.emrebabur.postflow.API_BASE_URL
import com.emrebabur.postflow.repo.comment.CommentApi
import com.emrebabur.postflow.repo.post.PostApi
import com.emrebabur.postflow.repo.user.UserApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class ApiModule {

    @Provides
    @Singleton
    fun retrofit(): Retrofit =
        Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(API_BASE_URL)
            .build()

    @Provides
    @Singleton
    fun postApi(retrofit: Retrofit): PostApi =
        retrofit.create(PostApi::class.java)

    @Provides
    @Singleton
    fun userApi(retrofit: Retrofit): UserApi =
        retrofit.create(UserApi::class.java)

    @Provides
    @Singleton
    fun commentApi(retrofit: Retrofit): CommentApi =
        retrofit.create(CommentApi::class.java)
}