package com.emrebabur.postflow.application

import com.emrebabur.postflow.repo.comment.BasicCommentRepo
import com.emrebabur.postflow.repo.comment.CommentApi
import com.emrebabur.postflow.repo.comment.CommentRepo
import com.emrebabur.postflow.repo.post.BasicPostRepo
import com.emrebabur.postflow.repo.post.PostApi
import com.emrebabur.postflow.repo.post.PostRepo
import com.emrebabur.postflow.repo.user.BasicUserRepo
import com.emrebabur.postflow.repo.user.UserApi
import com.emrebabur.postflow.repo.user.UserRepo
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepoModule {

    @Provides
    @Singleton
    fun postRepo(api: PostApi): PostRepo =
        BasicPostRepo(api)

    @Provides
    @Singleton
    fun userRepo(api: UserApi): UserRepo =
        BasicUserRepo(api)

    @Provides
    @Singleton
    fun commentRepo(api: CommentApi): CommentRepo =
        BasicCommentRepo(api)
}
