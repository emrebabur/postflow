package com.emrebabur.postflow.application

import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Named
import javax.inject.Singleton

@Module
class SchedulerModule {

    @Provides
    @Singleton
    @Named(KEY_SCHEDULER_IO)
    fun ioScheduler(): Scheduler =
        Schedulers.io()

    @Provides
    @Singleton
    @Named(KEY_SCHEDULER_MAIN)
    fun mainScheduler(): Scheduler =
        AndroidSchedulers.mainThread()

    companion object {
        const val KEY_SCHEDULER_IO = "key_scheduler_io"
        const val KEY_SCHEDULER_MAIN = "key_scheduler_main"
    }
}
