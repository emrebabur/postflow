package com.emrebabur.postflow.application

import com.emrebabur.postflow.application.SchedulerModule.Companion.KEY_SCHEDULER_IO
import com.emrebabur.postflow.application.SchedulerModule.Companion.KEY_SCHEDULER_MAIN
import com.emrebabur.postflow.repo.comment.CommentRepo
import com.emrebabur.postflow.repo.post.PostRepo
import com.emrebabur.postflow.repo.user.UserRepo
import dagger.Component
import io.reactivex.Scheduler
import javax.inject.Named
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ApiModule::class,
        RepoModule::class,
        SchedulerModule::class
    ]
)
interface AppComponent {

    fun postRepo(): PostRepo

    fun userRepo(): UserRepo

    fun commentRepo(): CommentRepo

    @Named(KEY_SCHEDULER_IO)
    fun ioScheduler(): Scheduler

    @Named(KEY_SCHEDULER_MAIN)
    fun mainScheduler(): Scheduler
}
