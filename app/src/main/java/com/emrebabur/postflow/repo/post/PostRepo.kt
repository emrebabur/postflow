package com.emrebabur.postflow.repo.post

import io.reactivex.Single

interface PostRepo {
    fun get(): Single<List<Post>>
    fun get(id: String): Single<Post>
}

class BasicPostRepo(private val api: PostApi) :
    PostRepo {

    override fun get(): Single<List<Post>> =
        api.getPosts()

    override fun get(id: String): Single<Post> =
        api.getPost(id).map { it.first() }
}
