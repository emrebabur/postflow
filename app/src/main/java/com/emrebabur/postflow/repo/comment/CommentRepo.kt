package com.emrebabur.postflow.repo.comment

import io.reactivex.Single

interface CommentRepo {
    fun get(postId: String): Single<List<Comment>>
}

class BasicCommentRepo(private val api: CommentApi) : CommentRepo {

    override fun get(postId: String): Single<List<Comment>> =
        api.getComments(postId)
}
