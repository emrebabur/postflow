package com.emrebabur.postflow.repo.user

data class User(
    val id: String,
    val name: String
)
