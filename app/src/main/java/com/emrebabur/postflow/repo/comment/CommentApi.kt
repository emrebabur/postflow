package com.emrebabur.postflow.repo.comment

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface CommentApi {
    @GET("/comments")
    fun getComments(@Query("postId") postId: String): Single<List<Comment>>
}
