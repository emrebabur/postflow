package com.emrebabur.postflow.repo.post

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface PostApi {
    @GET("/posts")
    fun getPosts(): Single<List<Post>>

    @GET("/posts")
    fun getPost(@Query("id") id: String): Single<List<Post>>
}
