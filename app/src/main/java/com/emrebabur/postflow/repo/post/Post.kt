package com.emrebabur.postflow.repo.post

data class Post(
    val id: String,
    val userId: String,
    val title: String,
    val body: String
)
