package com.emrebabur.postflow.repo.user

import io.reactivex.Single

interface UserRepo {
    fun get(id: String): Single<User>
}

class BasicUserRepo(private val api: UserApi) : UserRepo {
    override fun get(id: String): Single<User> =
        api.get(id).map { it.first() }
}
