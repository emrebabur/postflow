package com.emrebabur.postflow.repo.user

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface UserApi {
    @GET("/users")
    fun get(@Query("userId") id: String): Single<List<User>>
}
