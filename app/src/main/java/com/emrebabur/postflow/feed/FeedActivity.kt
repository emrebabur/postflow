package com.emrebabur.postflow.feed

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.emrebabur.postflow.R
import com.emrebabur.postflow.application.PostFlowApp
import com.emrebabur.postflow.feed.di.DaggerFeedComponent
import com.emrebabur.postflow.feed.di.FeedComponent
import com.emrebabur.postflow.plusAssign
import com.emrebabur.postflow.post.PostActivity
import com.emrebabur.postflow.wrap
import io.reactivex.disposables.CompositeDisposable

class FeedActivity : AppCompatActivity() {

    private lateinit var component: FeedComponent
    private lateinit var feedViewModel: FeedViewModel
    private lateinit var feedBinder: FeedBinder

    private val disposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        component = createComponent()
        val view = layoutInflater.inflate(R.layout.activity_post_feed, null)
        setContentView(view)
        setSupportActionBar(view.findViewById(R.id.post_feed_toolbar))
        feedViewModel = ViewModelProviders.of(this, component.feedViewModelFactory()).get(FeedViewModel::class.java)
        feedBinder = FeedBinder(FeedViewHolder(view))
    }

    override fun onStart() {
        super.onStart()

        disposable += feedViewModel.wrap()
            .subscribe(feedBinder)

        disposable += feedBinder.wrap()
            .subscribe {
                when (it) {
                    is FeedViewEvent.ItemClicked -> startPostActivity(it.id)
                }
            }
    }

    override fun onStop() {
        disposable.clear()
        super.onStop()
    }

    private fun startPostActivity(postId: String) {
        PostActivity.start(this, postId)
    }

    private fun createComponent(): FeedComponent =
        DaggerFeedComponent.builder()
            .appComponent(PostFlowApp.appComponent)
            .build()
}
