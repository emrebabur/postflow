package com.emrebabur.postflow.feed

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.emrebabur.postflow.plusAssign
import com.emrebabur.postflow.repo.post.PostRepo
import com.jakewharton.rxrelay2.BehaviorRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.ObservableSource
import io.reactivex.Observer
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable

class FeedViewModelFactory(
    private val postRepo: PostRepo,
    private val ioScheduler: Scheduler,
    private val mainScheduler: Scheduler
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        FeedViewModel(postRepo, ioScheduler, mainScheduler) as T
}

class FeedViewModel(
    postRepo: PostRepo,
    ioScheduler: Scheduler,
    mainScheduler: Scheduler
) : ViewModel(), ObservableSource<FeedViewModel.State> {

    private val disposable = CompositeDisposable()
    private val stateRelay: Relay<State> = BehaviorRelay.create()

    init {
        disposable += postRepo.get()
            .subscribeOn(ioScheduler)
            .observeOn(mainScheduler)
            .map { it.map { Post(it.id, it.title, it.body) } }
            .map { State.Displaying(it) as State }
            .toObservable()
            .startWith(State.Loading)
            .subscribe(stateRelay)
    }

    override fun onCleared() {
        disposable.clear()
        super.onCleared()
    }

    override fun subscribe(observer: Observer<in State>) {
        stateRelay.subscribe(observer)
    }

    sealed class State {
        object Loading : State()
        data class Displaying(val posts: List<Post>) : State()
    }
}
