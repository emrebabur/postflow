package com.emrebabur.postflow.feed

data class Post(
    val id: String,
    val title: String,
    val body: String
)
