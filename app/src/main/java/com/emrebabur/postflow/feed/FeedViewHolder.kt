package com.emrebabur.postflow.feed

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import com.emrebabur.postflow.R

class FeedViewHolder(view: View) {
    val recyclerView: RecyclerView = view.findViewById(R.id.post_feed_recyclerView)
    val loadingSpinner: ProgressBar = view.findViewById(R.id.post_feed_progressBar)
}
