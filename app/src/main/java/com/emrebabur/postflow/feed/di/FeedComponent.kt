package com.emrebabur.postflow.feed.di

import com.emrebabur.postflow.ActivityScope
import com.emrebabur.postflow.application.AppComponent
import com.emrebabur.postflow.feed.FeedViewModelFactory
import dagger.Component

@ActivityScope
@Component(
    dependencies = [AppComponent::class],
    modules = [FeedModule::class]
)
interface FeedComponent {

    fun feedViewModelFactory(): FeedViewModelFactory
}
