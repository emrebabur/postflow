package com.emrebabur.postflow.feed

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.emrebabur.postflow.R
import com.emrebabur.postflow.inflate
import com.jakewharton.rxrelay2.Relay

class PostAdapter(private val eventRelay: Relay<FeedViewEvent>) : RecyclerView.Adapter<PostAdapter.PostViewModelViewHolder>() {

    private var items: MutableList<Post> = mutableListOf()

    fun setItems(newItems: List<Post>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): PostViewModelViewHolder =
        PostViewModelViewHolder(parent.inflate(R.layout.post_feed_item_post), eventRelay)

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: PostViewModelViewHolder, position: Int) {
        holder.model = items[position]
    }

    inner class PostViewModelViewHolder(view: View, eventRelay: Relay<FeedViewEvent>) : RecyclerView.ViewHolder(view) {
        private val title = view.findViewById<TextView>(R.id.post_feed_postTitle)
        private val body = view.findViewById<TextView>(R.id.post_feed_postBody)

        init {
            view.setOnClickListener {
                model?.also {
                    eventRelay.accept(FeedViewEvent.ItemClicked(it.id))
                }
            }
        }

        var model: Post? = null
            set(value) {
                field = value?.also {
                    title.text = it.title
                    body.text = it.body
                }
            }
    }
}
