package com.emrebabur.postflow.feed

sealed class FeedViewEvent {
    data class ItemClicked(val id: String): FeedViewEvent()
}
