package com.emrebabur.postflow.feed

import android.support.v7.widget.LinearLayoutManager
import com.emrebabur.postflow.feed.FeedViewModel.State
import com.emrebabur.postflow.setVisibility
import com.jakewharton.rxrelay2.PublishRelay
import io.reactivex.ObservableSource
import io.reactivex.Observer
import io.reactivex.functions.Consumer

class FeedBinder(private val holder: FeedViewHolder) : Consumer<FeedViewModel.State>, ObservableSource<FeedViewEvent> {

    private val eventRelay = PublishRelay.create<FeedViewEvent>()
    private val layoutManager = LinearLayoutManager(holder.recyclerView.context)
    private val adapter = PostAdapter(eventRelay)

    init {
        holder.recyclerView.layoutManager = layoutManager
        holder.recyclerView.adapter = adapter
    }

    override fun accept(state: FeedViewModel.State) {
        holder.loadingSpinner.setVisibility(state is State.Loading)
        holder.recyclerView.setVisibility(state is State.Displaying)
        if(state is State.Displaying)
            adapter.setItems(state.posts)
    }

    override fun subscribe(observer: Observer<in FeedViewEvent>) {
        eventRelay.subscribe(observer)
    }
}
