package com.emrebabur.postflow.feed.di

import com.emrebabur.postflow.ActivityScope
import com.emrebabur.postflow.application.SchedulerModule
import com.emrebabur.postflow.feed.FeedViewModelFactory
import com.emrebabur.postflow.repo.post.PostRepo
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Named

@Module
class FeedModule {

    @Provides
    @ActivityScope
    fun feedViewModuleFactory(
        postRepo: PostRepo,
        @Named(SchedulerModule.KEY_SCHEDULER_IO) ioScheduler: Scheduler,
        @Named(SchedulerModule.KEY_SCHEDULER_MAIN) mainScheduler: Scheduler
    ): FeedViewModelFactory =
        FeedViewModelFactory(postRepo, ioScheduler, mainScheduler)
}
